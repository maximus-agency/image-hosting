Очень простой и примитивный хостинг картинок! Загружаем картинку, после чего получаем ссылку на оригинал, уменьшенную, стандартную и большую копии.

Для запуска используем команду `java -jar ./outPutName.jar --server.port=PORT` (например `java -jar ./image-hosting-0.0.1.jar --server.port=8080`)

Так же советую сделать proxy (пример на nginx):

```nginx
server {
    listen 80;
    listen 443 ssl;

    ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem; # managed by Certbot
    
    server_name ~^images\.;
    location / {
        proxy_cookie_domain	localhost 	     $host;
        proxy_set_header	Accept-Encoding  "";
        proxy_set_header   	Upgrade          $http_upgrade;
        proxy_set_header   	Connection       'upgrade';
        proxy_set_header 	Host             'images.example.com';
        proxy_set_header    X-Real-IP        $remote_addr;
        proxy_set_header    X-Forwarded-For  $proxy_add_x_forwarded_for;
        proxy_pass http://localhost:PORT_HERE/;
	}
}
```

Если вы знакомы с PM2 или его аналогами, то советую для отказоустойчивости использовать:
```
{
    "name": "image-hosting",
    "script": "/usr/bin/java",
    "args": [
        "-jar",
        "PROJECT_DIR_HERE/build/libs/outPutNameHere.jar"
    ],
    "exec_interpreter": "",
    "exec_mode": "fork"
}
```

Для теста можете использовать https://images.example.com/dev-upload

