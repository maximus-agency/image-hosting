package ru.ckateptb.imagehosting;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class ImageHosting {
    @SneakyThrows
    public static void main(String[] args) {
        SpringApplication.run(ImageHosting.class, args);
    }
}
