package ru.ckateptb.imagehosting.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ru.ckateptb.imagehosting.util.ImageUtils;

@RestController
public class ImageController {
    private final ImageUtils utils;

    public ImageController(ImageUtils utils) {
        this.utils = utils;
    }

    @GetMapping(value = "/{imageName}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public FileSystemResource getImage(@PathVariable("imageName") String imageName) {
        return new FileSystemResource(this.utils.getHomePath().resolve(imageName));
    }

    @GetMapping(value = "/dev-upload")
    public ModelAndView testUpload() {
        return new ModelAndView("index");
    }

    @PostMapping("/")
    @ResponseBody
    private UploadResponse uploadImage(MultipartFile file) {
        return utils.resizeAndSave(file);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UploadResponse {
        public String original;
        public String small;
        public String large;
        public String normal;
    }
}
