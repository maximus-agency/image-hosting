package ru.ckateptb.imagehosting.util;

import lombok.SneakyThrows;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ru.ckateptb.imagehosting.ImageHosting;
import ru.ckateptb.imagehosting.controller.ImageController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Path;
import java.util.UUID;

@Component
public class ImageUtils {
    private final ApplicationHome home;
    private final Path homePath;

    public ImageUtils() {
        this.home = new ApplicationHome(ImageHosting.class);
        this.homePath = Path.of(this.home.getDir().getPath(), "images");
    }

    @SneakyThrows
    public ImageController.UploadResponse resizeAndSave(MultipartFile file) {
        File homeDir = this.homePath.toFile();
        FileUtils.forceMkdir(homeDir);
        BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
        file.getContentType();
        String name = String.format("%s_%s", System.currentTimeMillis(), UUID.randomUUID());
        String originalName = String.format("%s_original", name);
        String smallName = String.format("%s_small", name);
        String normalName = String.format("%s_normal", name);
        String largeName = String.format("%s_large", name);
        Thumbnails.of(bufferedImage)
                .size(bufferedImage.getWidth(), bufferedImage.getHeight())
                .outputFormat("png")
                .outputQuality(1)
                .toFile(this.homePath.resolve(originalName).toFile());
        Thumbnails.of(bufferedImage)
                .width(150)
                .outputFormat("png")
                .outputQuality(1)
                .toFile(this.homePath.resolve(smallName).toFile());
        Thumbnails.of(bufferedImage)
                .width(600)
                .outputFormat("png")
                .outputQuality(1)
                .toFile(this.homePath.resolve(normalName).toFile());
        Thumbnails.of(bufferedImage)
                .width(1000)
                .outputFormat("png")
                .outputQuality(1)
                .toFile(this.homePath.resolve(largeName).toFile());
        return new ImageController.UploadResponse(originalName + ".png", smallName + ".png", largeName + ".png", normalName + ".png");
    }

    public ApplicationHome getHome() {
        return home;
    }

    public Path getHomePath() {
        return this.homePath;
    }
}
