package ru.ckateptb.imagehosting.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ExceptionHandlerAdvice {
    @ExceptionHandler
    public ResponseEntity<?> handleException(Throwable throwable) {
        log.debug(throwable.getMessage(), throwable);
        return ResponseEntity.notFound().build();
    }
}